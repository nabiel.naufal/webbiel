from django.http import HttpResponse
from django.shortcuts import render

# Create your views here.


def index(request):
    return render(request, 'home/index.html')


def portfo(request):
    return render(request, 'home/portfo.html')


def base(request):
    return render(request, 'base.html')
